package com.edd.document.act1;
/**
 * Copyright 2018, IES Maria Enriquez.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 *  in the documentation and/or other materials provided with the distribution.
 *  Neither the name of IES Maria Enriquez. nor the names of its
 *  contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * Point represent an point on the 2D coordinate Axis
 * <p>
 * An object of this class will represent a point on the negative or positive
 * plane in the infinite 2D coordinate AXIS
 *
 * @author Alex Coloma
 * @version 1.0
 */
public class Point {

    private double positionX;
    private double positionY;

    /**
     * Create a new Point object with x,y position
     * <p>
     * The point can belongs to both negative and positive plane
     * so x and y can be positive or negative
     *
     * @param positionX indicate the position on X axis
     * @param positionY indicate the position on Y axis
     */
    public Point(double positionX, double positionY) {

        this.positionX = positionX;
        this.positionY = positionY;

    }

    /**
     * Get X axis position of the point
     *
     * @return X axis position
     */
    public double getPositionX() {

        return positionX;

    }

    /**
     * Get Y axis position of the point
     *
     * @return X axis position
     */
    public double getPositionY() {

        return positionY;

    }

}
