package com.edd.document.act1;
/**
 * Copyright 2018, IES Maria Enriquez.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 *  in the documentation and/or other materials provided with the distribution.
 *  Neither the name of IES Maria Enriquez. nor the names of its
 *  contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/*
@param width specificate the longitude of the rectangle
@param height specificate the height of the rectangle

 */

public class Rectangle {

    private double width;
    private double height;
    private Point position;
    private double positionX;

    public Rectangle(double height, double width) {

        if ((height < 0.0 ) || (width < 0.0)) {
            throw new RuntimeException();
        }

        this.width = width;
        this.height = height;
        this.position = new Point(0.0,0.0);

    }

    public Rectangle(double height, double width, Point position) {

        this.width = width;
        this.height = height;
        this.position = position;

    }

    public double getPerimeter(){

        return this.width*2 + this.height*2;

    }
    /*
    @return the perimeter of the rectangle
     */

    public double getArea(){

        return this.width * this.height;

    }

    public Point getPosition(){

        return position;

    }

}
